#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 11 20:01:39 2019

@author: Gabriel Casas M.
@comment: Entrenamiento de dataset para obtener los thetas en el archivo thetaOut-N.npy
"""
from logisticRegression import LogisticRegression as lr
import numpy as np

# create X data from images in ../dataset
lrModel = lr()
X = lrModel.getDataset(originPath = '../dataset', imgSize = 80, example = 40, classes = 5)

y = np.array([np.concatenate((np.ones(40), np.zeros(160)), axis = 0), # Y for 0
              np.concatenate((np.zeros(40), np.ones(40), np.zeros(120)), axis = 0), # Y for 1
              np.concatenate((np.zeros(80), np.ones(40), np.zeros(80)), axis = 0), # Y for 4
              np.concatenate((np.zeros(120), np.ones(40), np.zeros(40)), axis = 0), # Y for 5
              np.concatenate((np.zeros(160), np.ones(40)), axis = 0)]) # Y for 8

i = 0
for yi in y:
    model = lr(alpha = 0.85, iterations = 700)
    model.fit(X, yi)
    preds = model.predict(X)
    print(preds)
    print("Training accuracy:" + str(100 * np.mean(preds == yi)) + "%")
    np.save('thetaOut-' + str(i), model.theta)
    i += 1
    print('================================')

print('\nFile name used for training\n')
print(lrModel.nameFile)