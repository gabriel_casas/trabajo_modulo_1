#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 11 22:11:01 2019

@author: Gabriel Casas M.
@comment: Predecir la imagenes de la carpeta ../dataset/test
"""
from logisticRegression import LogisticRegression as lr
import numpy as np
# predict images from test folder, using thetas into thetaOut-N.npy file
# replace N for 0, 1, 4, 5, 8 into name thetaOut-N.npy 
trainedTheta = np.load('./output/thetaOut-1.npy')
model = lr()
Xt = model.getTestDataset(originPath = '../dataset/test', imgSize = 80, example = 52)
testPreds = model.predict(X=Xt, theta = trainedTheta)
print(testPreds)
print(model.nameFile)

print('\nComparation, correct names predicted')
for i in range(testPreds.shape[0]):
    if (testPreds[i]):
        print('Name predicted: %s' % (model.nameFile[i]))
