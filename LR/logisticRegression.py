#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  8 19:50:29 2019

@author: Gabriel Casas M.
@comment: Logistic Regresion Model (One vs All)
          para el trabajo final de modulo 1 del Diplomado en Machine Learning
"""

import numpy as np
import matplotlib.pyplot as plt
import cv2
import os

class LogisticRegression:

    dataset = None
    def __init__(self, alpha=0.01, iterations=100):
        self.alpha = alpha
        self.iterations = iterations
        self.nameFile = []
    
    def getDataset(self, originPath = '../dataset', arrayFolders = ['0', '1', '4', '5', '8'], imgSize = 80, example = 40, classes = 5):
        if (os.path.exists(originPath)):
            dataset = np.zeros(((example * classes), imgSize * imgSize))
            self.nameFile = []
            i = 0
            for path in arrayFolders:
                if (path != 'test'):
                    for imagePath in os.listdir("%s/%s" % (originPath, path)):
                        fullPatImage = "%s/%s/%s" % (originPath, path, imagePath)
                        if (fullPatImage.find('jpg') > 0 or fullPatImage.find('JPG') >0):
                            image = cv2.imread(fullPatImage)
                            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                            resizedImage = cv2.resize(gray, (imgSize, imgSize))
                            dataset[i, :] = resizedImage.flatten()[np.newaxis]
                            self.nameFile.append(imagePath)
                            i += 1
                        else:
                            print("No se encontraron imagens .jpg o .JPG revise el directorio")
                            return None
            return dataset
        else:
            print("%s no es un directorio"%(originPath))
            return None
    def getTestDataset(self, originPath, imgSize = 80, example = 40):
        if (os.path.exists(originPath)):
            dataset = np.zeros((example, imgSize * imgSize))
            self.nameFile = []
            i = 0
            for imagePath in os.listdir(originPath):
                fullPatImage = "%s/%s" % (originPath, imagePath)
                if (fullPatImage.find('jpg') > 0 or fullPatImage.find('JPG') >0):
                    image = cv2.imread(fullPatImage)
                    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                    resizedImage = cv2.resize(gray, (imgSize, imgSize))
                    dataset[i, :] = resizedImage.flatten()[np.newaxis]
                    self.nameFile.append(imagePath)
                    i += 1
                else:
                    print("No se encontraron imagens .jpg o .JPG revise el directorio")
                    return None
            return dataset
        else:
            print("%s no es un directorio"%(originPath))
            return None

    def addConstant(self, X):
        b = np.ones((X.shape[0], 1))
        return np.concatenate((b, X), axis = 1)

    # h(x) signoid Function
    def signoid(self, x):
        return 1 / (1 + np.exp(-x))

    # J loss function
    def loss(self, h, y):
        return (-y * np.log(h) - (1 - y) * np.log(1 - h)).mean()

    def fit(self, X, y):
        X = self.addConstant(X)
        # weights initialization
        self.theta = np.zeros(X.shape[1])
        j = np.zeros((self.iterations))
        for i in range(self.iterations):
            z = np.dot(X, self.theta)
            # h(z)
            h = self.signoid(z)
            # update theta
            gradient = np.dot(X.T, (h - y))/y.size
            self.theta -= self.alpha * gradient
            # J
            z = np.dot(X, self.theta)
            h = self.signoid(z)
            j[i] = self.loss(h, y)
        """
        fig, ax = plt.subplots()
        t = np.arange(0.0, self.iterations, 1.0)
        ax.set(xlabel='iterations', ylabel='cost', title='logistic regression')
        ax.plot(t, j)
        ax.grid()
        plt.show()
        """

    def predict(self, X, theta = np.array([])):
        X = self.addConstant(X)
        if (theta.size > 0):
            print('using theta given...')
            self.theta = theta
        return self.signoid(np.dot(X, self.theta)) >= 0.5
