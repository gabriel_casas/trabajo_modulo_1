# Logistic Regression
predicción de números 0, 1, 4, 5, 8 dados en la carpeta **../dataset**
por: **Gabriel Casas Mamani**
 
**Nota.-** todo está definido por defecto para el proyecto y ejecutar los scripts, si se requiere especificar rutas o parámetros diferentes, modificarlos en los scripts directamente.
 
### Modelo
Todos los metodos necesarios estan en el archivo [logisticRegression.py](./logisticRegression.py) como clase.
 
### Entrenamiento
Para entrenar el modelo con la carpeta **../dataset** se hace correr el archivo [trainingDigitsLogisticRegression.py](./trainingDigitsLogisticRegression.py)
 
```
$ python3 trainingDigitsLogisticRegression.py
```
este creara los archivos:
 
thetaOut-0.npy     thetas para los 0's
thetaOut-1.npy     thetas para los 1's
thetaOut-2.npy     thetas para los 4's
thetaOut-3.npy     thetas para los 5's
thetaOut-4.npy     thetas para los 8's
 
La precisión obtenida en el entrenamiento son de:
* Training accuracy: **99.5%**   for 0
* Training accuracy: **99.5%**   for 1
* Training accuracy: **100.0%**  for 4
* Training accuracy: **100.0%**  for 5
* Training accuracy: **99.0%**   for 8

Los **thetas** para este trabajo estan en la carpeta [output/](./output) con los nombres thetaOut-0.npy, thetaOut-1.npy, thetaOut-4.npy, thetaOut-5.npy y thetaOut-8.npy

### Prueba
Para realizar las pruebas a los resultados del modelo se debe hacer correr el archivo [predictTestDigitsLogisticRegresion.py](./predictTestDigitsLogisticRegresion.py)
```
$ python3 predictTestDigitsLogisticRegresion.py
```
este archivo usara algún **theta** que se obtuvo en el entrenamiento de los datos.
para probarlos uno por uno se debe editar el archivo en la línea 13:
```python
trainedTheta = np.load('./output/thetaOut-10.npy')
```
y cambiar el nombre del archivo que contiene a los thetas **(thetaOut-0.npy, thetaOut-1.npy, thetaOut-4.npy, thetaOut-5.npy, thetaOut-8.npy)**
 
el script imprime:
* los resultados de la predicción (True False)
* los nombres de los archivos leídos(no siempre es en orden del gestor de archivos y se necesita comparar)
* una comparación de los resultados obtenidos y los nombres para mostrar aciertos.
 
La precisión obtenida en los test se calculó en base a las predicciones y la diferencia de los errores* lo que da:
* Test accuracy for 0: **100%**
* Test accuracy for 1: **83.34%**
* Test accuracy for 4: **83.34%**
* Test accuracy for 5: **90.0%**
* Test accuracy for 8: **91.66%**
 
*Las predicciones son 100% correctas pero el modelo equivoco y agregó más resultados a las predicciones entre 1 y 2
 
### Adicional creación de scripts
Se reformuló los scripts elaborados en clase en el archivo [logisticRegression.py](./logisticRegression.py) donde se tiene los métodos:
* **getDataset**
 devuelve un array con las imágenes de la carpeta dada, sus subcarpetas y su contenido
 parámetros:
 * **originPath** dirección del archivo contenedor de las imágenes en subcarpetas por defecto: **'../dataset'**
 * **arrayFolders** array con los nombres de las carpetas dentro de ../dataset que contienen a las imágenes .JPG, por defecto: **['0', '1', '4', '5', '8']**
 * **imgSize** tamaño de las imágenes se utilizará para redimensionar las mismas, por defecto: **80**
 * **example** cantidad de muestras o ejemplos con los que se trabajara, por defecto: **40**
 * **classes** cantidad de clases que se utilizarán, por defecto: **5**
 
* **getTestDataset**
 devuelve un array con las imágenes de la carpeta **test** y su contenido
 parámetros:
 * **originPath** dirección del archivo contenedor de las imágenes en subcarpetas por defecto: **'../dataset'**
 * **arrayFolders** array con los nombres de las carpetas dentro de ../dataset que contienen a las imágenes .JPG, por defecto: **['0', '1', '4', '5', '8']**
 * **imgSize** tamaño de las imágenes se utilizará para redimensionar las mismas, por defecto: **80**
 * **example** cantidad de muestras o ejemplos con los que se trabajara, por defecto: **40**
 * **classes** cantidad de clases que se utilizarán, por defecto: **5**
* **signoid** calcula la formula signoid
   $h(x) = \frac{1}{1 + e^-(\theta_x)}$
* **loss** calcula los costos de los pesos en J con la fórmula
 $J(\theta)=-\frac{1}{m}(-y^Tlog(hx) + (1-y^T)(log(1-hx)))$
 la actualización con gradiente se hace en el método fit
* **fit** entrena el modelo y devuelve los **thetas** para cada muestra en un archivo llamado thetaOut-N.npy
* **predict**
 predice una muestra usando los thetas obtenidos en el entrenamiento
