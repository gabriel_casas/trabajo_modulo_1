# Multi Layer Perceptron
predicción de números 0, 1, 4, 5, 8 dados en la carpeta **../dataset**
por: **Gabriel Casas Mamani**
 
**Nota.-** todo está definido por defecto para el proyecto y ejecutar los scripts, si se requiere especificar rutas o parámetros diferentes, modificarlos en los scripts directamente.
 
### Modelo
Todos los metodos necesarios estan en el archivo [multiLayerPerceptron.py](./multiLayerPerceptron.py) como clase.
 
### Entrenamiento
Para entrenar el modelo con la carpeta **../dataset** se hace correr el archivo [trainingDigitsMLP.py](./trainingDigitsMLP.py)
 
```
$ python3 trainingDigitsMLP.py
```
este creara los archivos:
 
weigth1.npy
weigth2.npy
 
La precisión obtenida en el entrenamiento es de:
* Training accuracy: **99.5%** 

Los **weigth** para este trabajo estan en la carpeta [output/](./output) con los nombres weigth1.npy y weigth2.npy

### Prueba
Para realizar las pruebas a los resultados del modelo se debe hacer correr el archivo [predictTestMLP.py](./predictTestMLP.py)
```
$ python3 predictTestMLP.py
```
este archivo usara los **weigth** que se obtuvo en el entrenamiento de los datos.
 
el script imprime:
* los resultados de la predicción como matriz
* los nombres de los archivos leídos(no siempre es en orden del gestor de archivos y se necesita comparar)
* una comparación de los resultados obtenidos y los nombres para mostrar aciertos.
 
La precisión obtenida en los test se calculó en base a las predicciones y la diferencia de los errores* lo que da:
* Test accuracy for 0: **100%**
 