#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  8 19:50:29 2019

@author: Gabriel Casas M.
@comment: Multi Layer Perceptron
          para el trabajo final de modulo 1 del Diplomado en Machine Learning
visit: https://medium.com/better-programming/how-to-build-2-layer-neural-network-from-scratch-in-python-4dd44a13ebba
"""

import numpy as np
import cv2
import os

class MultiLayerPerceptron:

    dataset = None
    def __init__(self, alpha=0.01, epoch=100, hiddenLayers = 5):
        self.nameFile = []
        self.alpha = alpha
        self.epoch = epoch
        self.hiddenLayers = hiddenLayers
        self.e = []
        self.weigth = {'w1': [], 'w2': []}
        self.a = {'a1': [], 'a2': []}
        self.z = {'z1': [], 'z2': []}
        self.grad = {'k': [], 'j': []}
        self.delta = {'d1': [], 'd2': []}

    def getDataset(self, originPath = '../dataset', arrayFolders = ['0', '1', '4', '5', '8'], imgSize = 80, example = 40, classes = 5):
        if (os.path.exists(originPath)):
            dataset = np.zeros(((example * classes), imgSize * imgSize))
            self.nameFile = []
            i = 0
            for path in arrayFolders:
                if (path != 'test'):
                    for imagePath in os.listdir("%s/%s" % (originPath, path)):
                        fullPatImage = "%s/%s/%s" % (originPath, path, imagePath)
                        if (fullPatImage.find('jpg') > 0 or fullPatImage.find('JPG') >0):
                            image = cv2.imread(fullPatImage)
                            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                            resizedImage = cv2.resize(gray, (imgSize, imgSize))
                            dataset[i, :] = resizedImage.flatten()[np.newaxis]
                            self.nameFile.append(imagePath)
                            i += 1
                        else:
                            print("No se encontraron imagens .jpg o .JPG revise el directorio")
                            return None
            return dataset
        else:
            print("%s no es un directorio"%(originPath))
            return None

    def getTestDataset(self, originPath, imgSize = 80, example = 40):
        if (os.path.exists(originPath)):
            dataset = np.zeros((example, imgSize * imgSize))
            self.nameFile = []
            i = 0
            for imagePath in os.listdir(originPath):
                fullPatImage = "%s/%s" % (originPath, imagePath)
                if (fullPatImage.find('jpg') > 0 or fullPatImage.find('JPG') >0):
                    image = cv2.imread(fullPatImage)
                    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                    resizedImage = cv2.resize(gray, (imgSize, imgSize))
                    dataset[i, :] = resizedImage.flatten()[np.newaxis]
                    self.nameFile.append(imagePath)
                    i += 1
                else:
                    print("No se encontraron imagens .jpg o .JPG revise el directorio")
                    return None
            return dataset
        else:
            print("%s no es un directorio"%(originPath))
            return None

    #Bias as 1
    def addConstant(self, X):
        b = np.ones((X.shape[0], 1))
        return np.concatenate((b, X), axis = 1)
    #Activation function
    def signoid(self, x):
        return 1 / (1 + np.exp(-x))

    def fit(self, X, y):
        X = self.addConstant(X)
        X = X.astype(float) / 255
        (m, n) = X.shape
        self.outLayers = y.shape[1]
        self.weigth['w1'] = (np.random.rand(self.hiddenLayers, n) * 4.8 - 2.4) / (n + 1)
        self.weigth['w2'] = (np.random.rand(self.outLayers, self.hiddenLayers+1) * 4.8 - 2.4) / (self.hiddenLayers + 1)
        for j in range(0, self.epoch):
            for i in range(0, m):
                # forward propagation
                self.a['a1'] = X[i, :]
                self.z['z1'] = self.signoid(np.dot(self.a['a1'], self.weigth['w1'].T))
                self.a['a2'] = np.ones((self.z['z1'].shape[0] + 1))
                self.a['a2'][1:self.hiddenLayers + 1] = self.z['z1']
                self.z['z2'] = self.signoid(np.dot(self.a['a2'], self.weigth['w2'].T))
                # back propagation
                self.e = y[i, :] - self.z['z2']
                self.grad['k'] = self.z['z2'] * (1 - self.z['z2']) * self.e
                self.delta['d2'] = self.alpha * np.dot(self.grad['k'][np.newaxis].T, self.a['a2'][np.newaxis])
                self.grad['j'] = self.z['z1'] * (1 - self.z['z1']) * (np.dot(self.grad['k'][np.newaxis], self.weigth['w2'][:, 1:]))
                self.delta['d1'] = self.alpha * np.dot(self.grad['j'].T, self.a['a1'][np.newaxis])
                self.weigth['w1'] = self.weigth['w1'] + self.delta['d1']
                self.weigth['w2'] = self.weigth['w2'] + self.delta['d2']
    
    def prediction(self, X, hiddenLayers=5, w1=np.array([]), w2=np.array([])):
        X = self.addConstant(X)
        X = X.astype(float) / 255
        self.pred = np.zeros((200,5))
        (m, n) = X.shape
        if (w1.size > 0):
            print('using w1 given...')
            self.weigth['w1'] = w1
            self.pred = np.zeros((52,5))
        if (w2.size > 0):
            print('using w2 given...')
            self.weigth['w2'] = w2
            self.pred = np.zeros((52,5))
        for i in range(0, m):
            a1 = X[i, :]
            z1 = self.signoid(np.dot(a1, self.weigth['w1'].T))
            a2 = np.ones((z1.shape[0] + 1))
            a2[1:hiddenLayers + 1] = z1
            self.pred[i] = self.signoid(np.dot(a2, self.weigth['w2'].T)) >= 0.5
        return self.pred

