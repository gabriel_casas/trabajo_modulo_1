#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 24 20:49:22 2019

@author: Gabriel Casas M.
@comment: Entrenamiento de dataset para obtener los weight_2 en el archivo weight_2Out-N.npy
"""
from multiLayerPerceptron import MultiLayerPerceptron as mlp
import numpy as np

# create X data from images in ../dataset
model = mlp()
X = model.getDataset(originPath = '../dataset', imgSize = 80, example = 40, classes = 5)

y = np.array([np.concatenate((np.ones(40), np.zeros(160)), axis = 0), # Y for 0
              np.concatenate((np.zeros(40), np.ones(40), np.zeros(120)), axis = 0), # Y for 1
              np.concatenate((np.zeros(80), np.ones(40), np.zeros(80)), axis = 0), # Y for 4
              np.concatenate((np.zeros(120), np.ones(40), np.zeros(40)), axis = 0), # Y for 5
              np.concatenate((np.zeros(160), np.ones(40)), axis = 0)]) # Y for 8
print(y.T)
model = mlp(alpha=0.05, epoch=2000, hiddenLayers = 5)
model.fit(X, y.T)

pred = model.prediction(X, hiddenLayers = 5)
print (pred)

print("Training accuracy:" + str(100 * np.mean(pred == y.T)) + "%")
np.save('./output/weigth1', model.weigth['w1'])
np.save('./output/weigth2', model.weigth['w2'])
