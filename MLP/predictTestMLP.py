#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec  1 20:18:48 2019

@author: gabriel
@comment: Predecir la imagenes de la carpeta ../dataset/test
"""
from multiLayerPerceptron import MultiLayerPerceptron as mlp
import numpy as np

# predict images from test folder, using thetas into thetaOut-N.npy file
# replace N for 0, 1, 4, 5, 8 into name thetaOut-N.npy 
w1_load = np.load('./output/weigth1.npy')
w2_load = np.load('./output/weigth2.npy')

model = mlp(hiddenLayers = 5)
Xt = model.getTestDataset(originPath = '../dataset/test', imgSize = 80, example = 52)
testPreds = model.prediction(X=Xt, hiddenLayers=5, w1=w1_load, w2=w2_load)

print(testPreds)
print(model.nameFile)

print('\nComparation, correct names predicted')
for i in range(testPreds.shape[0]):
    numberPredicted = ''
    val = np.where(testPreds[i] == 1)
    #print(val)

    if val[0].size > 0 and val[0] == 0:
        numberPredicted = 'Cero  '
    if val[0].size > 0 and val[0] == 1:
        numberPredicted = 'Uno   '
    if val[0].size > 0 and val[0] == 2:
        numberPredicted = 'Cuatro'
    if val[0].size > 0 and val[0] == 3:
        numberPredicted = 'Cinco '
    if val[0].size > 0 and val[0] == 4:
        numberPredicted = 'Ocho  '
    print('with array %s predict %s :::: for file name: %s' % (str(testPreds[i]), numberPredicted, model.nameFile[i]))
