# trabajo_modulo_1

Codigo para el trabajo del primer modulo del diplomado
Gabriel Casas Mamani.

El proyecto esta dividido en 2 carpetas
[LR (Logistic Regression)](./LR) y la carpeta [MLR (Multi Layer Perceptron)](./MLR) en ambas se tiene el un README para la documentacion de cada uno

* [LR-README.md](./LR/LR-README.md)
* [MLP-README.md](./MLR/MLP-README.md)

ambos utilizan la misma carpeta **dataset** 